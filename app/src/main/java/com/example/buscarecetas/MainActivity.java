package com.example.buscarecetas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private TextView ingredientes, titulo;
    private Button btBuscar;
    private EditText nombre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.btBuscar = (Button) findViewById(R.id.btBuscar);
        this.ingredientes = (TextView) findViewById(R.id.ingredientes);
        this.nombre = (EditText)findViewById(R.id.nombre);
        this.titulo = (TextView)findViewById(R.id.titulo);

        this.btBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String r= nombre.getText().toString();

                String url ="https://api.edamam.com/search?q=" + r + "&app_id=c3379671&app_key=e06ba4d28e82dc3f67eac8f24a37d40e";

                StringRequest solicitud  = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    Toast.makeText(getApplicationContext(), "hola", Toast.LENGTH_SHORT).show();
                                    JSONObject respuestaJSON = new JSONObject(response);
                                    JSONArray hits = respuestaJSON.getJSONArray("hits");
                                    JSONObject receta1= hits.getJSONObject(0);
                                    JSONObject receta = receta1.getJSONObject("recipe");

                                    String nom = receta.getString("label");

                                    JSONArray in = receta.getJSONArray("ingredientLines");
                                    String n ="";

                                    titulo.setText(nom);

                                    for (int i=0; i<in.length();i++){
                                        n= n +" - "+in.getString(i)+"\n";
                                    }


                                    ingredientes.setText(n);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);

            }
        });



    }
}
